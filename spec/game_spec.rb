require_relative '../lib/game'
require_relative '../lib/renderer/text_renderer'

module GameOfLife

  BOARD_WIDTH = 50
  BOARD_HEIGHT = 50
  STARTING_LIVING_CELLS = 200

  RSpec.describe Game, "create new board" do
    it "should be able to create an empty board" do
      expect(Game.empty_board()).to be_instance_of Hash
    end

    it "should be able to create a random board" do
      num_of_live_cells = 20
      board = Game.random_board(20, 10, num_of_live_cells)
      expect(board).to be_instance_of Hash
      expect(board.size).to eq(num_of_live_cells)
    end
  end

  RSpec.describe Game, "game related" do
    it "should have a board when game is created" do
      game = Game.new(hor: BOARD_WIDTH, ver: BOARD_HEIGHT,
                      num: STARTING_LIVING_CELLS, renderer: TextRenderer.new)
      expect(game.board).to be
    end

    it "should be able to create boards of any size and number of cells" do
      hor = rand(200)
      ver = rand(200)
      num = rand([hor, ver].min)
      game = Game.new(hor: hor, ver: ver, num: num, renderer: TextRenderer.new())
      expect(game.board).to be
      expect(game.board.size).to eq(num)
    end

    it "should know how many cells around a cell are alive" do
      hor = 3
      ver = 3
      num = 9
      game = Game.new(hor: hor, ver: ver, num: num, renderer: TextRenderer.new())
      expect(game.surrounding_live_cells([1,1])).to eq(9)
      expect(game.surrounding_live_cells([0,1])).to eq(6)
      expect(game.surrounding_live_cells([1,0])).to eq(6)
      expect(game.surrounding_live_cells([0,0])).to eq(4)
      num = 0
      game = Game.new(hor: hor, ver: ver, num: num, renderer: TextRenderer.new())
      expect(game.surrounding_live_cells([1,1])).to eq(0)
    end

    it "should kill those live cells not surrounded by 2 or 3 live cells" do
      hor = 3
      ver = 3
      num = 9
      game = Game.new(hor: hor, ver: ver, num: num, renderer: TextRenderer.new())
      game.print_board
      game.iterate
      game.print_board
      expect(game.live_cells_count).to eq(4)
      game.iterate
      game.print_board
      expect(game.live_cells_count).to eq(0)
    end

    it "should turn cells into life those surrounding by 3 live cells" do
      hor = 5
      ver = 5
      num = 0
      game = Game.new(hor: hor, ver: ver, num: num, renderer: TextRenderer.new())
      Game.resurrect(game.board, [1,0])
      Game.resurrect(game.board, [1,1])
      Game.resurrect(game.board, [0,1])
      game.print_board
      expect(game.live_cells_count).to eq(3)

      game.iterate
      game.print_board
      expect(game.live_cells_count).to eq(4)
    end
  end
end

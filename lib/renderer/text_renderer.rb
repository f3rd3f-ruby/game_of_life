module GameOfLife
  class TextRenderer
    def initialize(*)
      puts 'Using Text Renderer'
    end

    def render(board:, width:, height:)
      puts ''
      width = *(0...width)
      height = *(0...height)
      height.each do |row|
        width.each do |column|
          print board[[column, row]] == 1 ? '⬛' : '⬜'
          $stdout.flush
        end
        puts ''
      end
    end
  end
end

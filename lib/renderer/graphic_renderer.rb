require 'sdl2'

module GameOfLife
  class GraphicRenderer

    LIVE_COLOR = [255, 0, 0].freeze
    DEAD_COLOR = [155, 155, 155].freeze

    def initialize(width:, height:)
      SDL2.init(SDL2::INIT_VIDEO)
      @window = SDL2::Window.create('Game of Life',
                                    SDL2::Window::POS_CENTERED,
                                    SDL2::Window::POS_CENTERED,
                                    width, height, 0)
      @renderer = @window.create_renderer(-1, 0)
      @window_width = width
      @window_height = height
    end

    def render(board:, width:, height:)
      tile_size = (@window_width / width)

      width = *(0...width)
      height = *(0...height)

      height.each do |row|
        width.each do |column|
          @renderer.draw_color = board[[column, row]] == 1 ? LIVE_COLOR : DEAD_COLOR
          @renderer.fill_rect(SDL2::Rect.new(column * tile_size,
                                             row * tile_size,
                                             tile_size - 1, tile_size - 1))
        end
      end
      @renderer.present
    end
  end
end

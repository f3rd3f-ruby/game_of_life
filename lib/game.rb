module GameOfLife
  class Game
    attr_accessor :board
    attr_reader :rounds

    def initialize(hor:, ver:, num:, renderer:)
      @width = hor
      @height = ver
      @original = num

      @board = Game.random_board(hor, ver, num)
      @rounds = 0

      @renderer = renderer
    end

    def iterate
      new_cells = Game.empty_board

      cells = all_cells
      cells.each do |x, y|
        surrounding = surrounding_live_cells([x, y])
        if alive([x,y]) # live cells
          Game.resurrect(new_cells, [x, y]) if [3, 4].include?(surrounding)
        elsif surrounding == 3 # dead cells
          Game.resurrect(new_cells, [x, y])
        end
      end
      set_board(new_cells)
      @rounds += 1
    end

    def set_board(board)
      @board = board.dup
    end

    def alive(cell)
      @board[cell] == 1
    end

    def live_cells_count
      @board.size
    end

    def any_alive?
      live_cells_count.positive?
    end

    def surrounding_live_cells(cell)
      x, y = cell
      rhor = case x
             when 0
               (x..x + 1)
             when @width - 1
               (x - 1..x)
             else (x - 1..x + 1)
             end
      rver = case y
             when 0
               (y..y + 1)
             when @height - 1
               (y - 1..y)
             else (y - 1..y + 1)
             end

      hor = *rhor
      ver = *rver

      prod = hor.product(ver)
      prod.select do |cell|
        @board[cell] == 1
      end.size
    end

    def all_cells
      width = *(0...@width)
      height = *(0...@height)
      width.product(height)
    end

    def print_board
      render_board = Game.complete_board(live_cells: board,
                                         width: @width,
                                         height: @height)

      @renderer.render(board: render_board, width: @width, height: @height)
    end

    class << self
      def empty_board
        Hash.new(0)
      end

      def random_board(size_x, size_y, num_of_live_cells)
        board = empty_board()
        (0...num_of_live_cells).each do
          x = y = 0
          loop do
            x = rand(size_x)
            y = rand(size_y)
            break if board[[x, y]].zero?
          end
          board[[x, y]] = 1
        end
        board
      end

      def complete_board(live_cells:, width:, height:)
        board = empty_board()

        width = *(0...width)
        height = *(0...height)

        height.each do |row|
          width.each do |column|
            board[[column, row]] = live_cells.include?([column, row]) ? 1 : 0
          end
        end

        board
      end

      def resurrect(board, cell)
        board[cell] = 1
      end
    end
  end
end

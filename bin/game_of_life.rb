require_relative '../lib/game'
require_relative '../lib/renderer/graphic_renderer'

BOARD_WIDTH = 50
BOARD_HEIGHT = 50
STARTING_LIVING_CELLS = 200

def main
  renderer = GameOfLife::GraphicRenderer.new(width: 800, height: 800)
  game = GameOfLife::Game.new(hor: BOARD_WIDTH, ver: BOARD_HEIGHT,
                              num: STARTING_LIVING_CELLS, renderer: renderer)
  while game.any_alive?
    game.print_board
    game.iterate
    sleep(1)
  end
  puts "Rounds #{game.rounds}"
end

puts 'Welcome to Game of Life'

main
